<?php get_header(); ?>
<div class="page-wrap">

    <section class="banner">

        <div class="contentp" id="content5" data-stellar-background-ratio="0.90" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/assets/images/banner.jpg'); 
    background-repeat: no-repeat;
    min-height: 180px;
    text-align:center;
    background-position: center!important;
    ">
            <div class="center-content">
                <h1><?php esc_html_e('Tax investigation insurance', 'wlctest'); ?></h1>
            </div>
        </div>
    </section>

    <div class="container">
        <main>
            <div class="grid">
                <div class="col-8_sm-12">
                    <section class="content">
                        <h2>Tax investigation insurance</h2>
                        <p>
                            <strong>Free tax investigation cover, should you need it HM Revenue & Customs (HMRC)
                                routinely
                                check
                                a proportion of tax returns to make sure they are correct. They may also decide to
                                conduct
                                an
                                extensive examination of all areas of your tax affairs including an in-depth review of
                                your
                                tax
                                return records.</strong><br><br>

                            These enquiries take time and money to resolve. Tax Investigation Insurance is designed to
                            remove
                            the burden should you be subject to an investigation. The policy indemnifies you for
                            professional
                            costs whilst you are being represented. <a class="color-link" href="#">Terms and
                                conditions</a>
                            apply.<br><br>

                            <a class="color-link" href="#">How much does it cost?</a><br>
                            Insurance cover can cost over £100, but for UKALA members it is included as part of your
                            membership
                            of the UK Association of Letting Agents (UKALA). <a class="color-link" href="#">Terms and
                                conditions</a> apply.
                        </p>
                    </section>
                </div>
                <div class="col-4_sm-12">
                    <aside class="sidebar">
                        <img class="clipboard"
                            src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/clipboard.svg"
                            alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" />
                        <hr>
                        <p>
                            Additional Tax investigation insurance documents and useful information
                        </p>
                    </aside>
                </div>
            </div>
            <hr>
            <article class="members">
                <h3>members section</h3>
                <h2>Members tax investigation insurance</h2>
                <p><strong>
                        Free tax investigation cover, should you need it HM Revenue & Customs (HMRC) routinely check a
                        proportion of tax returns to make sure they are correct. They may also decide to conduct an
                        extensive examination of all areas of your tax affairs including an in-depth review of your tax
                        return records.</strong>

                </p>
                <br>
                <p>
                These enquiries take time and money to resolve. Tax Investigation Insurance is designed to remove the burden should you be subject to an investigation. The policy indemnifies you for professional costs whilst you are being represented. Terms and conditions apply.
                </p>
<br><br>

            </article>
<section class="become">

<h3>Become a UKALA member for full access</h3>
<p>Join UKALA today and gain full access to all our membership benefits, support and accreditations</p>
<a href="#"><button>Join now</button></a>
<p>Already a member? Log in</p>
</section>
        </main>

    </div>


    <section class="popular">
        <h4 class="center">Popular links</h4>
        <div class="container">
        <div class="flex-popular">
            <div class="popular-items">
            Join UKALA
            </div>
            <div class="popular-items">
            Search for a UKALA agent
            </div>
            <div class="popular-items">
            Contact us
            </div>
        </div>
        </div>
    </section>


</div>
<?php get_footer(); ?>