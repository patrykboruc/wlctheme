<footer class="main-footer">
    <div class="container">
        <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo-site"
                            src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/logo.svg"
                            alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" /></a>
    </div>
</footer>


<script src="https://cdnjs.cloudflare.com/ajax/libs/stellar.js/0.6.2/jquery.stellar.min.js"></script>
<?php wp_footer(); ?>
</body>

</html>