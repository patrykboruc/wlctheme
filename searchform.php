<div class="search-section">
<form method="get" id="searchform" action="<?php echo esc_url(home_url('/') . pll_current_language() . '/'); ?>">
<div class="search-box">
<input type="text" class="search-txt" name="s" id="s" placeholder="<?php pll_e('Search', 'wlctest'); ?>" />
<button type="submit" class="search-btn" id="searchsubmit" ><i class="fas fa-search"></i></button>
</div>
</form>
</div>
